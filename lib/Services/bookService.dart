import 'package:tp_1/Models/Book.dart';

class BookService {
  Future <List<Book>> getBooks() async {
    BooksSingleton();
    await Future.delayed(Duration(seconds: 1));
    final List<Book> books = BooksSingleton.books;
    return books;
  }
}