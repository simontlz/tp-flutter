import 'package:flutter/material.dart';
import 'package:tp_1/Models/Book.dart';
import 'package:tp_1/Services/bookService.dart';

import 'Widgets/BookListView.dart';

void main() => runApp(BookApp());

class BookApp extends StatefulWidget {
  @override
  BookAppState createState() => new BookAppState();
}

class BookAppState extends State<BookApp> {
  List<Book> _books;

  @override
  initState() {
    super.initState();
    // Add listeners to this class
    getBooks();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Book App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to the new Book listing App'),
          backgroundColor: Colors.redAccent,
        ),
        body: Center(
          child: _books == null
              ? Center(child: CircularProgressIndicator())
              : BookListView(books: _books),
        ),
      ),
    );
  }

  getBooks() async {
    List<Book> books = await BookService().getBooks();
    setState(() {
      _books = books;
    });
  }
}


