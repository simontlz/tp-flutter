import 'package:flutter/material.dart';
import 'package:tp_1/Models/Book.dart';

import 'BookDetails.dart';

class BookListView extends StatelessWidget {
  final List<Book> books;
  BookListView({this.books});

  @override
  Widget build(BuildContext context) {
    return Container(child: ListView(children: bookItem(context)));
  }

  List<Widget> bookItem(BuildContext context) {
    var booksContainer = List<Widget>();
    if (books != null) {
      final colors = [Colors.amber[600], Colors.green[600], Colors.blue[600]];
      books.forEach((Book book) {
        booksContainer.add(ListTile(
          title: Text('${book.title}',
              style: TextStyle(fontWeight: FontWeight.bold)),
          subtitle: Text('${book.description}',
              overflow: TextOverflow.ellipsis, maxLines: 2),
          isThreeLine: true,
          leading: CircleAvatar(
            backgroundColor: colors[book.id - 1],
            child: Text(
              "${book.authorFirstName.substring(0, 1)}${book.authorLastName.substring(0, 1)}",
              style: TextStyle(color: Colors.white),
            ),
          ),
          trailing: FlatButton(
            color: Colors.blue[300],
            textColor: Colors.white,
            splashColor: Colors.blue[600],
            child: Text("DÉTAIL"),
            onPressed: () =>
                pushBookDetailScreen(book, colors[book.id - 1], context),
          ),
        ));
      });
    }
    return booksContainer;
  }

  pushBookDetailScreen(Book book, Color color, BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => BookDetails(book: book, primaryColor: color)),
    );
  }
}