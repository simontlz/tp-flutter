import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:tp_1/Models/Book.dart';

class BookDetails extends StatelessWidget {
  final Book book;
  final Color primaryColor;
  BookDetails({this.book, this.primaryColor});

  @override
  Widget build(BuildContext firstContext) {
    return MaterialApp(
      title: 'Book details',
      home: Scaffold(
          appBar: AppBar(
              title: Text(book.title.toUpperCase()),
              backgroundColor: primaryColor,
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    onPressed: () => Navigator.pop(firstContext),
                  );
                },
              )),
          body: SafeArea(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                        left: 50, right: 50, top: 20, bottom: 20),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: CachedNetworkImage(
                          fit: BoxFit.fill,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          imageUrl: book.imageUrl,
                        )),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      FloatingActionButton(
                        onPressed: () {},
                        heroTag: "emailButton",
                        child: Icon(Icons.email),
                        backgroundColor: primaryColor,
                      ),
                      FloatingActionButton(
                        onPressed: () {},
                        heroTag: "shareButton",
                        child: Icon(Icons.share),
                        backgroundColor: primaryColor,
                      ),
                      FloatingActionButton(
                        onPressed: () {},
                        heroTag: "likeButton",
                        child: Icon(Icons.favorite),
                        backgroundColor: primaryColor,
                      ),
                    ],
                  ),
                  Expanded(
                      flex: 1,
                      child: Container(
                          margin: EdgeInsets.only(
                              top: 30, left: 30, right: 30),
                          child: SingleChildScrollView(
                              child: ConstrainedBox(
                            constraints: BoxConstraints(
                              minHeight: MediaQuery.of(firstContext).size.width,
                            ),
                            child: Text(book.extract,
                                overflow: TextOverflow.visible,
                                textAlign: TextAlign.justify),
                          ))))
                ]),
          )),
    );
  }
}